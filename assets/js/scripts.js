$(document).ready(function(e){
	
	$('.item-lightbox a#open-lightbox').click(function(){
		
		var openImage	= $(this).attr('data-lightbox');
		var box		= '<div class="box-opening-lightbox"><div class="block-lightbox"><a href="javascript:void(0)" class="close-lightbox" id="close-lightbox"></a><img src="'+openImage+'"/></div></div>';
		
		$('.lightbox').append(box);
		
		$('.lightbox a#close-lightbox').click(function(){
			$('.lightbox .box-opening-lightbox').remove();
		});
		
		return false;
		
	});
	
});